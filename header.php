<html>
<head>
	<title><?php bloginfo('name'); wp_title('|'); ?></title>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

</head>
<body <?php body_class(); ?> >
	<div id="wrapper">
		<div id="header">
			<h1><?php bloginfo('name'); ?></h1>
			<h3><?php bloginfo('description') ?></h3>
		</div>