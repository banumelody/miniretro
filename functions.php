<?php

// post formats
$post_formats = array( 
	'aside',
	'gallery', 
	'link', 
	'image', 
	'quote', 
	'status', 
	'video', 
	'audio', 
	'chat' );
add_theme_support( 'post-formats', $post_formats);	
// end post format

// thumbnails
$post_thumbnails = array(
	'post',
	'page',
	'attachment',
	'revision',
	'nav_menu_item',
	'movie');
add_theme_support( 'post-thumbnails', $post_thumbnails );
// end thumbnails

// background
$default_background = array(
	'default-color' => 'abcdef');
add_theme_support( 'custom-background' );
// end background

// custom header
$default_header = array(
	'default-image'          => '',
	'random-default'         => true,
	'width'                  => 0,
	'height'                 => 0,
	'flex-height'            => true,
	'flex-width'             => true,
	'default-text-color'     => '000000',
	'header-text'            => true,
	'uploads'                => true,
	'wp-head-callback'       => '',
	'admin-head-callback'    => '',
	'admin-preview-callback' => '',
	);
add_theme_support( 'custom-header', $default_header );
// end custom header

add_theme_support( 'automatic-feed-links' );
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form' ) );

function my_theme_add_editor_styles() {
	add_editor_style( 'custom-editor-style.css' );
}
add_action( 'init', 'my_theme_add_editor_styles' );


// widget
function arphabet_widgets_init() {

	register_sidebar(array(
		'name' => __( 'Right Hand Sidebar' ),
		'id' => 'right-sidebar',
		'description' => __( 'Widgets in this area will be shown on the right-hand side.' ),
		'before_title' => '<h1>',
		'after_title' => '</h1>'
		));
}
add_action( 'widgets_init', 'arphabet_widgets_init' );
// end widget

?>